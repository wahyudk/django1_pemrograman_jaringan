from django.shortcuts import render
from . import models
# Create your views here.


def index(request):
    dataMhs = models.Mahasiswa.objects.all()

    context = {'judul':'Web Mahasiswa', 'pageName':'Halaman Tampil Mahasiswa', 'kontirbutor': 'Wahyu Dwi Krisnanto, Mohamad Rifqi Ramadhan', 'Mahasiswa':dataMhs}
    
    return render(request, 'mahasiswa/index.html', context)
