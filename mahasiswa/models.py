from django.db import models

# Create your models here.
from django.contrib.auth.models import User

class Mahasiswa(models.Model):
    JK_CHOICES = (('l','Pria'),('w', 'wanita'))
    PS_CHOICES = (('mi', 'manajemen informatika'),('ak', 'akuntasi'),('me', 'mesin'))

    nama = models.CharField('Nama Mahasiswa', max_length=50)
    alamat = models.CharField(max_length=100)
    jk = models.CharField('kelamin', max_length=1, choices=JK_CHOICES)
    ps = models.CharField('Program Studi', max_length=2, choices=PS_CHOICES)
    nim = models.CharField(max_length=30)
    no_tlp = models.CharField('No.Hp', max_length=30, blank=True)
    email = models.CharField('E-Mail', max_length=100, blank=True)

    def __unicode__(self):
        return self.nama
